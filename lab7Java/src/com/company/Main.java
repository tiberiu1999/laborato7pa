package com.company;

public class Main {

    public static void main(String[] args) {
        Game game = new Game();
        Board board=new Board(15);
        game.setBoard(board);
        Player player1=new Player("Player 1");
        Player player2=new Player("Player 2");
        game.addPlayer( player1);
        game.addPlayer( player2);

        game.addPlayer(new Player("Player 3"));
        game.addPlayer(new Player("Player 4"));
        game.addPlayer(new Player("Player 5"));
        game.start();


    }
}