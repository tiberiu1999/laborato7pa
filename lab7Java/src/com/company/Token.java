package com.company;

import java.util.Random;

public class Token {
    int token;

    public void setToken(int token) {
        this.token = token;
    }

    public Token() {
        Random rand = new Random();
        token = rand.nextInt(535);
    }

    public int getToken() {
        return token;
    }
}