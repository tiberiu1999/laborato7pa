package com.company;

import java.util.ArrayList;
import java.util.List;

public class Player implements Runnable{
    private String name;
    private Game game;
    List<Integer> List = new ArrayList<>();

    public Player(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public void run() {
        while (List.size() < 5) {
            if (List.isEmpty()) {
                List.add(game.getBoard().getTokenBoard());
            }
            int x = game.getBoard().getTokenBoard();
            if (List.get(List.size() - 1) < x) {
                List.add(x);
            } else {
                List.clear();
            }

        }

        System.out.println(this.getName() + " : " + List);
    }
}