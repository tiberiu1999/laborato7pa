package com.company;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Game {
    private Board board;
    private List<Thread> threads;
    private final List<Player> players = new ArrayList<>();

    public void addPlayer(Player player){
        players.add(player);
        player.setGame(this);
    }


    public void start(){
        threads  = new LinkedList<>();
        for (Player player: players) {
            Thread thread = new Thread(player);
            thread.start();
            threads.add(thread);
        }

    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public List<Player> getPlayers() {
        return players;
    }
}
