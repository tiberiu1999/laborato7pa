package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Collections;

public class Board {
    int nrTokens;
    List<Integer> tokens = new ArrayList<>();


    public Board(int nrTokens) {
        this.nrTokens=nrTokens;
        Token token;
        for(int i = 1; i <=nrTokens; i++){
            Token tk = new Token();
            tokens.add(tk.getToken());
        }
        Collections.shuffle(tokens);
    }

    public boolean isEmpty() {
        if(tokens.isEmpty())
            return true;
        else
            return false;
    }

    public synchronized int getTokenBoard(){
        Random rand = new Random();

        int index = rand.nextInt(tokens.size());
        return tokens.get(index);
    }

    public int getNumberOfTokens() {
        return nrTokens;
    }
    public List<Integer> getTokens() {
        return tokens;
    }
    public void setNumberOfTokens(int nrTokens) {
        this.nrTokens = nrTokens;
    }
}